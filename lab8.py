from multiprocessing import Process, Pipe
from datetime import datetime

# first, let's define some functions we will use:


def send_message(pipe, pid, logical_time_vector):
    """
    function to send message from one process to another through the pipe

    """
    logical_time_vector[pid] += 1
    pipe.send(('Some message', logical_time_vector))
    print(f'Message sent from {pid} {local_time(logical_time_vector)}')
    return logical_time_vector


def local_time(logical_time_vector):
    # function that just returns logical time vector and local time in printable way
    return f'(logical_time_vector={logical_time_vector}, time={datetime.now()})'


def event(pid, logical_time_vector):
    logical_time_vector[pid] += 1
    print(f'Something hapenned in {pid} {local_time(logical_time_vector)}!')
    return logical_time_vector


def recv_message(pipe, pid, logical_time_vector):
    """
    function to receive message from some process in another through the pipe

    """
    message, timestamp = pipe.recv()
    logical_time_vector = calc_recv_timestamp(timestamp, logical_time_vector)
    print('Message received at ' + str(pid) + local_time(logical_time_vector))
    logical_time_vector[pid] += 1
    return logical_time_vector


def calc_recv_timestamp(recv_time_stamp, logical_time_vector):
    """
    Calculate timestamp at message receive

    The function takes the maximum of the received timestamp
    and its local logical_time_vector, and increments it with one.
    """

    for id in range(3):
        recv_time_stamp[id] = max(recv_time_stamp[id], logical_time_vector[id])
    return recv_time_stamp

# Then, define our processes


class Process1(Process):
    def __init__(self, pipe12):
        super(Process, self).__init__()
        self.PID = 0
        self.pipe12 = pipe12
        self.logical_time_vector = [0, 0, 0]

    def run(self):
        self.logical_time_vector = send_message(
            self.pipe12,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = send_message(
            self.pipe12,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = event(self.PID, self.logical_time_vector)
        self.logical_time_vector = recv_message(
            self.pipe12,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = event(
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = event(
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = recv_message(
            self.pipe12,
            self.PID,
            self.logical_time_vector,
        )
        print(self.logical_time_vector)


class Process2(Process):
    def __init__(self, pipe21, pipe23):
        super(Process, self).__init__()
        self.PID = 1
        self.pipe21 = pipe21
        self.pipe23 = pipe23
        self.logical_time_vector = [0, 0, 0]

    def run(self):
        self.logical_time_vector = recv_message(
            self.pipe21,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = recv_message(
            self.pipe21,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = send_message(
            self.pipe21,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = recv_message(
            self.pipe23,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = event(self.PID, self.logical_time_vector)
        self.logical_time_vector = send_message(
            self.pipe21,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = send_message(
            self.pipe23,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = send_message(
            self.pipe23,
            self.PID,
            self.logical_time_vector,
        )
        print(self.logical_time_vector)


class Process3(Process):
    def __init__(self, pipe32):
        super(Process, self).__init__()
        self.PID = 2
        self.pipe32 = pipe32
        self.logical_time_vector = [0, 0, 0]

    def run(self):
        self.logical_time_vector = send_message(
            self.pipe32,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = recv_message(
            self.pipe32,
            self.PID,
            self.logical_time_vector,
        )
        self.logical_time_vector = event(self.PID, self.logical_time_vector)
        self.logical_time_vector = recv_message(
            self.pipe32,
            self.PID,
            self.logical_time_vector
        )
        print(self.logical_time_vector)


# Now, let's run our processes
pipe12, pipe21 = Pipe()
pipe23, pipe32 = Pipe()
process1 = Process1(pipe12)
process2 = Process2(pipe21, pipe23)
process3 = Process3(pipe32)
process1.start()
process2.start()
process3.start()
process1.join()
process2.join()
process3.join()
